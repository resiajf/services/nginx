# nginx for the web

The repository creates the basic configuration for the web server. Uses an `nginx`
image based on Debian 9 (aka stretch) and adds the configuration needed to work
with an PHP-FPM instance.

## Configuration files

There's two files that makes nginx behave like it does. But, what is it's
behaviour?

Is a simple HTTP server with a PHP backend (with `php-fpm`). Everything it
founds in the folder `/var/www/html` is served directly. If the file requested
ends with `.php`, sends the request to the `php-fpm` instance
(`tcp://php-fpm:9000`) to show the output through the PHP engine.

### default.conf

The site configuration. Sets the specific configuration for the WordPress site.

### fastcgi-php.conf

Specific configuration to make the `php-fpm` engine to work with nginx.

## Volumes

 - `/var/www/html`: The web folder
 - `/var/cache/nginx`: Cache folder of nginx

 > **Warning**: The files of the web folder must be owned by `www-data` (UID 33)
 if not, it won't show anything or will show 403 Forbidden errors. To accomplish
 that, you can simply run `sudo chown -R 33:33 /path/to/web/folder/*`.

 > **Warning**: If a file shows a 403 error, maybe it has the incorrect
 permissions. Should have u+rwx g+rwx a+rx. Change them with
 `sudo chmod -R a+rw /path/to/web/folder/*` (for example).