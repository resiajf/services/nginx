FROM nginx:latest

COPY default.conf /etc/nginx/conf.d/default.conf
COPY fastcgi-php.conf /etc/nginx/

EXPOSE 80
